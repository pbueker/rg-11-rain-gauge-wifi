/***

Copyright (c) 2021 Patrick Büker

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <ArduinoOTA.h>
#include <SoftwareSerial.h>
#include <MQTT.h>
#include <ArduinoJson.h>



#define MQTT_IP "192.168.2.135"

#define INITIAL_UPDATE_RATE 22
#define INITIAL_UPDATE_RATE_SLOW 45

WiFiManager wifiManager;
WiFiClient net;
MQTTClient mqttClient(185);

#define RainTx D1
#define RainRx D2

#define RGBits_OFFSET 6
#define SLOWREG_OFFSET 8

SoftwareSerial SerialRain(RainRx, RainTx, true);
/* char read from serial port*/
char c;
/*frame, constructed from serial port data, always starts with s*/
char frame[31] = "------------------------------";
/*old data extracted from frame, updated wen next frame is complete and fully processed*/
uint8_t frame_o[24];
/*for all registers and bits, report changes immediately*/
uint8_t change_note[4];
uint8_t update_rate = INITIAL_UPDATE_RATE;  //rate in number of complete frames
uint8_t update_countdown = update_rate;
uint8_t update_rate_slow = INITIAL_UPDATE_RATE_SLOW; //rate of slow register  value  update rate in frames
uint8_t update_countdown_slow = update_rate_slow;


uint8_t fi;   //frame pointer
uint16_t fc;  //frame counter
uint8_t reg_val[7];
const char* reg_names[7] = {
  "PeakRS",
  "SPeakRS",
  "RainAD8",
  "LRA",
  "TransRat",
  "AmbLNoise",
  "RgBits"
};

const char* bit_names[8] = {
  "PkOverThr",
  "Raining",
  "Out1On",
  "HtrOn",
  "IsDark",
  "Cndnstn",
  "Freeze",
  "Storm"
};

const char* sreg_names[16] = {
  "RevLevel",
  "EmLevel",
  "RecEmStr",
  "ABLevel",
  "TmprtrF",
  "PUGain",
  "ClearTR",
  "AmbLight",
  "Bucket",
  "Barrel",
  "RGConfig",
  "DwellT",
  "SinceRn",
  "MonoStb",
  "LightAD",
  "RainThr"
};


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("...");
  Serial.println("Ready");

  wifiManager.setConfigPortalTimeout(180);
  wifiManager.autoConnect();
  /*
    while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
    }
  */
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed!");
    delay(600);
  }

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname("esp8266-rg11");

  // Authentication to prevent accidental update to the wrong ESP board
  ArduinoOTA.setPassword("rg11-admin");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
  Serial.println("Set");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Go");
  pinMode(RainRx, INPUT);

  //MQTT init

  mqttClient.begin(MQTT_IP, net);
  mqttClient.onMessage(messageReceived);
  Serial.print("\nconnecting...");
  while (!mqttClient.connect("******", "***", "***")) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println("");
  connect();

  SerialRain.begin(1200);
  Serial.println("Begin sampling");

  change_note[0] = 255; // register
  change_note[1] = 255; // binary values
  change_note[2] = 255; // slow register
  change_note[3] = 255; // slow register
}

void loop() {
  // put your main code here, to run repeatedly:

  mqttClient.loop();
  ArduinoOTA.handle();
  if (!mqttClient.connected()) {
    connect();
  }

  if (SerialRain.available()) {
    c = SerialRain.read();
    //Serial.write(c);
    if (c == 's') {
      uint8_t vald = 0;
      fi = 0;
      update_countdown--;
      update_countdown_slow--;
      mqttClient.publish("/sensor/rg11/debug/frame", frame);
      Serial.println("...-");
      Serial.println(frame);
      // Print out Registers
      for (int i = 0; i < 6; i++) {
        vald = getRegVal(frame, i);
        if (vald != frame_o[i]) {
          //value has changed
          if ((change_note[0] & (1 << i)) > 0) {
            char buf[32];
            char vbuf[4];
            const char *tpk = "/sensor/rg11/changed/";
            strcpy(buf, tpk);
            strcat(buf, reg_names[i]);
            sprintf(vbuf, "%i", vald);
            mqttClient.publish(buf, vbuf);
          }
          Serial.print(reg_names[i]);
          Serial.print(" ");
          Serial.print(vald);
          Serial.println();
        }
        frame_o[i] = vald;
      }
      Serial.print("---");
      Serial.print(reg_names[RGBits_OFFSET]);
      Serial.print(":");
      Serial.print(frame[RGBits_OFFSET * 2 + 1]);
      Serial.print(frame[RGBits_OFFSET * 2 + 2]);
      Serial.println();
      // Print out state (binary) data
      for (int i = 0; i < 8; i++) {
        char mask = 1;
        uint8_t bval;
        uint8_t bvalo;
        char inform = 0;
        mask = mask << i;
        bval = getRegVal(frame, RGBits_OFFSET);

        if (((change_note[1] & (1 << i)) > 0) && ((mask & bval) != (mask & frame_o[RGBits_OFFSET]))) {
          bvalo = frame_o[i];
          char buf[32];
          char vbuf[4];
          const char *tpk = "/sensor/rg11/changed/";
          strcpy(buf, tpk);
          strcat(buf, bit_names[i]);
          sprintf(vbuf, "%i", (mask & bval) >> i);
          mqttClient.publish(buf, vbuf);
        }
        frame_o[RGBits_OFFSET] = bval;
        bval = (mask & bval) > 0;
        if (bval) {
          Serial.print(bit_names[i]);
          Serial.println(": on");
        }
      }
      if (update_rate > 0 && update_countdown <= 0) {
        Serial.println(F("DynamicJsonDocumentDynamicJsonDocumentDynamicJsonDocumentDynamicJsonDocument"));
        DynamicJsonDocument doc(256);
        for (int i = 0; i < 6; i++) {
          doc[reg_names[i]] = frame_o[i];
        }
        for (int i = 0; i < 8; i++) {
          char mask = 1;
          mask = mask << i;
          doc[bit_names[i]] = ((mask & frame_o[RGBits_OFFSET]) > 0);
        }
        char buffer[500];
        size_t wby;
        wby = serializeMsgPack(doc, buffer, 500);
        Serial.println(buffer);
        Serial.println(wby);
        mqttClient.publish("/sensor/rg11/data", buffer, wby);
        update_countdown = update_rate;
      }


      uint8_t sregi;
      uint8_t sval;
      //Print slow register values SLOWREG_OFFSET
      sregi = getRegVal(frame, SLOWREG_OFFSET - 1);
      if (sregi < 16) {
        sval = getRegVal(frame, SLOWREG_OFFSET);
        if (frame_o[sregi + 7] != sval) {
          Serial.println("---");
          Serial.print(sreg_names[sregi]);
          Serial.print("(");
          Serial.print(sregi);
          Serial.print(")");
          Serial.print(":");
          Serial.print(sval);
          Serial.println();
        }
        uint8_t cnof = (sregi / 8) + 2; // offset of chande note arr
        if ((frame_o[sregi + 7] != sval) && ((( 1 << (sregi % 8)) & change_note[cnof])  > 0)) {
          char buf[32];
          char vbuf[4];
          const char *tpk = "/sensor/rg11/changed/";
          strcpy(buf, tpk);
          strcat(buf, sreg_names[sregi]);
          sprintf(vbuf, "%i", sval);
          mqttClient.publish(buf, vbuf);
        }
        frame_o[sregi + 7] = sval;
      } else {
        Serial.println("##ERR##");
      }
      for (int i = 0; i < 30; i++) {
        frame[i] = '-';
      };
      if (update_rate_slow > 0 && update_countdown_slow <= 0) {
        Serial.println(F("DynamicJsonDocumentDynamicJsonDocumentDynamicJsonDocumentDynamicJsonDocument"));
        DynamicJsonDocument doc(512);
        for (int i = 0; i < 16; i++) {
          doc[sreg_names[i]] = frame_o[i + 7];
        }
        char buffer[500];
        size_t wby;
        wby = serializeMsgPack(doc, buffer, 500);
        Serial.println(buffer);
        Serial.println(wby);
        mqttClient.publish("/sensor/rg11/data", buffer, wby);
        update_countdown_slow = update_rate_slow;

      }
    }
    if (fi < 28) {
      frame[fi] = c;
      fi++;
    }
  }
}


uint8_t getRegVal(char* frame, uint8_t pos) {
  uint8_t cc1 = 0;
  if ((((char)frame[pos * 2 + 1]) - 48) >= 0 && (((char)frame[pos * 2 + 1]) - 48) <= 10) {
    cc1 = (((uint8_t)frame[pos * 2 + 1]) - 48) << 4;
  } else if ((((char)frame[pos * 2 + 1]) - 97) >= 0 && (((char)frame[pos * 2 + 1]) - 97) <= 5) {
    cc1 = ((((uint8_t)frame[pos * 2 + 1]) - 97) + 10) << 4;
  }
  if ((((char)frame[pos * 2 + 2]) - 48) >= 0 && (((char)frame[pos * 2 + 2]) - 48) <= 10) {
    cc1 |= (((uint8_t)frame[pos * 2 + 2]) - 48);
  } else if ((((char)frame[pos * 2 + 2]) - 97) >= 0 && (((char)frame[pos * 2 + 2]) - 97) <= 5) {
    cc1 |= (((uint8_t)frame[pos * 2 + 2]) - 97) + 10;
  }
  return cc1;
}


void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  StaticJsonDocument<200> doc;
  deserializeMsgPack( doc, payload);
  if (doc.containsKey("update_rate")) {
    Serial.print("update_rate: ");
    Serial.println((uint8_t) doc["update_rate"]);
    update_rate = doc["update_rate"];
    Serial.println(update_rate);
  }
  if (doc.containsKey("update_rate_slow")) {
    Serial.print("update_rate_slow: ");
    Serial.println((uint8_t) doc["update_rate_slow"]);
    update_rate_slow = doc["update_rate_slow"];
    Serial.println(update_rate_slow);
  }
  

  // Note: Do not use the client in the callback to publish, subscribe or
  // unsubscribe as it may cause deadlocks when other things arrive while
  // sending and receiving acknowledgments. Instead, change a global variable,
  // or push to a queue and handle it in the loop after calling `client.loop()`.
}

void connect() {
  Serial.print("checking wifi...");

  if (!WiFi.isConnected()) { 
      int conn_result = WiFi.waitForConnectResult(10000); // connect try it for 10 seconds
      Serial.print("conn_result: ");
      Serial.println(conn_result, DEC);
  }
  if (WiFi.status() != WL_CONNECTED) {
    wifiManager.autoConnect();
  }

  Serial.print("\nconnecting...");
  int recon_i = 5;
  while(recon_i > 0 && !mqttClient.connected() && !mqttClient.connect("arduino", "try", "try")) {
    Serial.print(".");
    delay(100);
    recon_i--;
  }

  Serial.println("\nconnected!");
  mqttClient.subscribe("/sensor/rg11/command");
}
