# RG-11 Rain Gauge Wifi

Gather Data from a Hydreon Rain Gauge Model RG-11 using Ardunino and a ESP8266 (WEMOS D1 Mini)  using the serial protocol and send it via MQTT.

## Usage
 The Device starts an access point if it cannot connect to the Wifi, you can then connect to it, open your webbrowser and use the Wifi Manager to supply the settings. For now the MQTT settings cannot be provided via the WifiManager, but that is on the roadmap.
### MQTT
The Device publishes data from the RG-11 rain gauge in regular intervals in msgpack (https://msgpack.org/) format to the topic **/sensor/rg11/data**. The intervals can be configured for the slow registers and normal registers being sent. Additionally you can configure the Device to publish data if values in the registers change. Those changes are published to the **/sensor/rg11/changed/{value}** topic. The {value} is replaced with the name of the register and the value is in the payload as text. The update rate can be configured via MQTT using topic **/sensor/rg11/command** and a msgpack object with *update_rate* or *update_rate_slow* key and an integer value representing the number of updates the sensor sends via RS232.
### OTA Updates
The device can be updated OTA so you can mount it without having to worry about wiring it to a programmer for updates.
### Wiring
Attach Pin D2 of your ESP to the Raingauge as shown in the sketch. 
![Pins of the RG-11](/doc/Schema_RG-11_Pins2.png)

You have to use a voltage divider and together with the blue LED you can see that data is coming from the rain gauge. The RS232 signal is inverted, the code takes care of that. If you use the power shield for the WEMOS D1 mini you can power both deices from the same +12V supply.

## Libraries
   * 256dpi/arduino-mqtt by Joel Gaehwiler (Joël Gähwiler)
   * jandrassy/ArduinoOTA by Arduino,Juraj Andrassy
   * arduinojson by Benoit Blanchon

## Roadmap
   * Add configuration web interface
   * Make payload format for MQTT configurable as msgpack (defaut) or json
   * Change Format for MQTT change-messages to msgpack/json
   * Add error detection and report them as debug mesages
   * Add RSSI or other statistical data to additional debug messages

## Contributing
Contributions as well as bug reports are welcome. Contributors agree that all code and documentation is released under the MIT license.

## License
MIT License
